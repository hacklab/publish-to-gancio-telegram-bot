#!/usr/bin/env python
# pylint: disable=unused-argument, wrong-import-position
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Application and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import re
import os
import logging
import requests

from dotenv import load_dotenv
from telegram import __version__ as TG_VER
from datetime import datetime

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 5):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )
from telegram import ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

load_dotenv()  # Load environment variables from .env file
telegram_token = os.getenv('TELEGRAM_TOKEN')
gancio_url = os.getenv('GANCIO_URL')

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

TITLE, DESCRIPTION, IMAGE, PLACE_NAME, PLACE_ADDRESS, DATE, TIME, TAGS = range(8)

class Event:
    title: str
    description: str
    start_datetime: int
    place_name: str
    place_address: str
    image_url: str
    tags: list

    def __init__(self,title,description,start_datetime,place_name,place_address):
        self.title = title
        self.description = description
        self.start_datetime = start_datetime
        self.place_name = place_name
        self.place_address = place_address
        self.image_url = ''
        self.tags = []

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Starts the conversation and asks the user for the event title."""
    print(context.user_data)
    context.user_data['event'] = Event(
        "Título por defecto", 
        "Descripción por defecto", 
        datetime.now().timestamp(),
        "Lugar por defecto",
        "Dirección por defecto"
    )

    await update.message.reply_text(
        "Buenas compa! ✊🏾✊🏿✊🏻 \n Este bot te ayudará a publicar un evento en mad.convoca.la en 8 pasos:\n"
        "✏️ 1. Título del evento\n"
        "📝 2. Descripción\n"
        "📷 3. Foto del cartel\n"
        "📍 4. Nombre del sitio\n"
        "📍 5. Dirección del sitio\n"
        "📅 6. Fecha\n"
        "⏰ 7. Hora\n"
        "🏷 8. Etiquetas (recuerda consultar la política de etiquetas la web)\n"
        "Utiliza el comando /cancel para detener la publicación.\n\n"
        "Empezamos, dime el título del evento."
    )

    return TITLE


async def title(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected title and asks for a decription."""
    logger.info("El título seleccionado es: \"%s\"", update.message.text)
    context.user_data['event'].title = update.message.text
    await update.message.reply_text(
        "📝 Seguimos con la descripción."
    )

    return DESCRIPTION


async def description(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected description and asks for a photo."""
    logger.info("La descripción guardada es: \"%s\"", update.message.text)
    context.user_data['event'].description = update.message.text
    await update.message.reply_text(
        "La foto del cartel porfi. \n📷 📷 📷"
    )

    return IMAGE


async def image(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the photo and asks for a location."""
    print("El objeto actualización")
    print(update)
    if update.message.photo:
        logger.info("Es una foto")
        image_file = await update.message.photo[-1].get_file()
        context.user_data['event'].image_url = image_file.file_path
        print(image_file)
    
    await update.message.reply_text(
        "Ya queda menos! \n\n"
        "🎤 Ahora necesitamos el nombre del sitio, ejemplo: La Enredadera CSOA \n"
    )

    return PLACE_NAME

async def placeName(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the location and asks for some info about the user."""
    logger.info("Nombre del lugar: %s", update.message.text)
    context.user_data['event'].place_name = update.message.text
    await update.message.reply_text(
        "📍 Vamos con la dirección del sitio, se mostrará tal y como la escribas. \n"
    )

    return PLACE_ADDRESS

async def placeAddress(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the location and asks for some info about the user."""
    logger.info("Dirección del lugar: %s", update.message.text)
    context.user_data['event'].place_address = update.message.text
    await update.message.reply_text(
        "Vamos con la fecha 📅. \n"
        "Necesitamos que la escribas con este formato dd/mm/aaaa (día/mes/año).\n"
        "Si el día o el mes solo tienen un dígito pon un 0 delante, ejemplo: 01/04/2022"
    )

    return DATE

async def date(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    dateFormat = re.compile("^(0[1-9]|[1-2][0-9]|3[0-1])/(0[1-9]|1[0-2])/([0-9]{4})$")
    # If format is correct...
    if dateFormat.match(update.message.text):
        """Stores the date and asks for the time."""
        logger.info("La fecha es: %s", update.message.text)
        context.user_data['event'].start_datetime = datetime.strptime(update.message.text, "%d/%m/%Y").timestamp()
        await update.message.reply_text(
            "La hora de inicio ⏰. \n"
            "En formato hh:mm (24h)"
        )

        return TIME
    
    # Otherwise...
    """Notifies the format was incorrect and let the user try again."""
    await update.message.reply_text(
        "Lo siento, el formato no es el adecuado, recuerda: dd/mm/aaaa (día/mes/año).\n"
        "Si el día o el mes solo tienen un dígito pon un 0 delante, ejemplo: 01/04/2022.\n"
        "Inténtalo de nuevo:"
    )

    return DATE

async def time(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    timeFormat = re.compile("^(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])$")

    # If format is correct...
    if timeFormat.match(update.message.text):
        """Stores the time and asks for the tags."""
        logger.info("La hora es: %s", update.message.text)
        dateObj = datetime.fromtimestamp(context.user_data['event'].start_datetime)
        timeObj = datetime.strptime(update.message.text, "%H:%M")
        context.user_data['event'].start_datetime = datetime.combine(dateObj.date(),timeObj.time()).timestamp()
        print(context.user_data['event'].start_datetime)
        await update.message.reply_text(
            "Último paso! (es opcional, si quieres saltártelo escribe '/skip')\n\n"
            "🏷 Con las etiquetas puedes mejorar la visibilidad de tu evento.\n"
            "Escribe las etiquetas SIN la almohadilla '#', es decir, solo palabras separadas por un espacio, ejemplo: 'tetuan laEnredadera okupacion'. \n"
            "Utiliza una etiqueta de distrito para que se muestre automáticamente en la colección de tu distrito, etiquetas temáticas por si alguien sigue los eventos por temas, por nombres de colectivos...\n"
            "Lo guay de mad.convoca.la tusae"
        )

        return TAGS

    # Otherwise...
    """Notifies the format was incorrect and let the user try again."""
    await update.message.reply_text(
        "Lo siento, el formato no es el adecuado, recuerda: hh:mm (24h).\n"
        "Ejemplo: 08:36, sería un horario de mañana. 20:36 sería un horario de tarde \n"
        "Inténtalo de nuevo:"
    )

    return TIME

async def tags(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the tags and asks publish the event."""
    logger.info("Las etiquetas son: %s", update.message.text)
    context.user_data['event'].tags = update.message.text.split()


    event = context.user_data['event']
    print(event.__dict__)

    # Send the POST request with the event object as JSON payload
    headers = {
        "Content-Type": "application/json"
    }
    response = requests.post(gancio_url, headers=headers, json=event.__dict__)
    if response.status_code == 200:
        print(response)
        await update.message.reply_text(
            "Publicando! \n"
            "Pide a alguna persona administradora que acepte tu evento. \n"
            "Una vez hecho comprueba el canal de Telegram https://t.me/madconvocala para verlo! 🔥🔥🔥"
        )
    else:
        print(response)
        print(response.text)
        await update.message.reply_text(
            "Lo sentimos, algo ha fallado, error:\n"
            f"{response.text}\n"
            "Contacta con el hacklab de la Ferroviaria https://hacklab.frama.io"
        )

    return ConversationHandler.END


async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    await update.message.reply_text(
        "Nos vemos en otro momento!", reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END


# async def skip_photo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
#     """Skips the photo and asks for a location."""
#     user = update.message.from_user
#     logger.info("User %s did not send a photo.", user.first_name)
#     await update.message.reply_text(
#         "I bet you look great! Now, send me your location please, or send /skip."
#     )

#     return LOCATION

def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(telegram_token).build()

    # Add conversation handler with the states GENDER, IMAGE, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            TITLE: [MessageHandler(filters.TEXT & ~filters.COMMAND, title)],
            DESCRIPTION: [MessageHandler(filters.TEXT & ~filters.COMMAND, description),],
            IMAGE: [MessageHandler(filters.ALL & ~filters.COMMAND, image)],
            PLACE_NAME: [MessageHandler(filters.TEXT & ~filters.COMMAND, placeName)],
            PLACE_ADDRESS: [MessageHandler(filters.TEXT & ~filters.COMMAND, placeAddress)],
            DATE: [MessageHandler(filters.TEXT & ~filters.COMMAND, date)],
            TIME: [MessageHandler(filters.TEXT & ~filters.COMMAND, time)],
            TAGS: [MessageHandler(filters.TEXT & ~filters.COMMAND, tags)],
            # DESCRIPTION: [MessageHandler(filters.IMAGE, photo), CommandHandler("skip", skip_photo)],
            # TIME: [MessageHandler(filters.Regex("^(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])$"), time)],
        },
        fallbacks=[CommandHandler("cancel", cancel)]
    )

    application.add_handler(conv_handler)

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()